from typing import TYPE_CHECKING, Optional
from dbus_next.signature import Variant
from dbus_next.constants import PropertyAccess
from dbus_next.aio.message_bus import MessageBus
from dbus_next.service import ServiceInterface, method, signal, dbus_property

import uuid

from . import errors
from .session import Session
from .collection import Collection
if TYPE_CHECKING:
    from .store import Store, ID, LABEL, ALIAS
    from .item import Item

class Service(ServiceInterface):

    def __init__(self, bus: MessageBus, store: "Store"):
        super().__init__("org.freedesktop.Secret.Service")

        self.bus   = bus
        self.path  = "/org/freedesktop/secrets"
        self.store = store

        self.loaded      = False
        self.aliases     : dict[ALIAS, ID]      = {}
        self.collections : dict[ID, Collection] = {}
        self.sessions    : dict[ID, Session]    = {}

    def init(self) -> "Service":
        if self.loaded:
            return self

        self.loaded = True
        collections = self.store.get_collections()
        print(f"Initializing service with {len(collections)} collection(s)")
        for id, aliases in collections.items():
            self.collections[id] = Collection(self, id).init()
            for alias in aliases:
                self.aliases[alias] = id
        return self

    def __str__(self) -> str:
        return "<Service>"

    def alias_to_path(self, alias):
        return self.path + "/aliases/" + alias

    def path_parts(self, path: str) -> list[str]:
        if path.startswith(self.path + "/"):
            return path.removeprefix(self.path + "/").split("/")
        return []

    def path_to_collection(self, path: str) -> Optional[Collection]:
        parts = self.path_parts(path)
        if len(parts) == 2:
            if parts[0] == "collection":
                return self.collections.get(parts[1])
            elif parts[0] == "aliases":
                id = self.aliases.get(parts[1])
                if id is not None:
                    return self.collections.get(id)

    def path_to_item(self, path: str) -> Optional["Item"]:
        parts = self.path_parts(path)
        if len(parts) == 3 and parts[0] == "collection":
            try:
                return self.collections[parts[1]].items[parts[2]]
            except:
                pass

    def path_to_session(self, path: str) -> Optional["Session"]:
        parts = self.path_parts(path)
        if len(parts) == 2 and parts[0] == "session":
            return self.sessions.get(parts[1])

    def export(self):
        self.bus.export(self.path, self)
        self.export_aliases()
        self.export_collections()

    def export_collections(self):
        for collection in self.collections.values():
            collection.export()

    def export_aliases(self):
        for alias, id in self.aliases.items():
            self.export_alias(alias, id)

    def export_alias(self, alias, id):
        self.bus.export(self.alias_to_path(alias), self.collections[id])

    def unexport(self):
        self.bus.unexport(self.path, self)
        self.unexport_aliases()
        self.unexport_collections()
        self.unexport_sessions()

    def unexport_collections(self):
        for collection in self.collections.values():
            collection.unexport()

    def unexport_aliases(self):
        for alias, id in self.aliases.items():
            self.unexport_alias(alias, id)

    def unexport_alias(self, alias, id):
        self.bus.unexport(self.alias_to_path(alias), self.collections[id])

    def unexport_sessions(self):
        for session in self.sessions.values():
            session.unexport()

    @method()
    def OpenSession(self, algorithm: "s", input: "v") -> "vo": # type: ignore
        print(f"Open session '{algorithm}'")
        id = uuid.uuid4().hex
        input: Variant = input
        session = Session(self, id, algorithm, input.value)
        session.export()
        print(f"Exported {session} at '{session.path}'")
        self.sessions[session.id] = session
        return [Variant("ay", session.pub_key or b""), session.path]

    @method()
    def CreateCollection(self, properties: "a{sv}", alias: "s") -> "oo": # type: ignore
        prompt = "/"

        aliases = set[str]()
        if alias != "":
            if alias in self.aliases:
                id = self.aliases[alias]
                return [self.collections[id].path, prompt]
            aliases.add(alias)

        properties : dict[str, Variant] = properties
        label      : LABEL = properties.get(Collection.LABEL, Variant("s", "")).value

        print(f"Create collection '{label}'")
        id = self.store.create_collection(label, aliases)
        collection = Collection(self, id)
        collection.export()
        print(f"Exported {collection} at '{collection.path}'")
        self.collections[id] = collection
        for alias in aliases:
            self.export_alias(alias, id)
            print(f"Aliased {collection} at '{self.alias_to_path(alias)}'")
            self.aliases[alias] = id

        self.CollectionCreated(collection)
        return [collection.path, prompt]

    @method()
    def SearchItems(self, attributes: "a{ss}") -> "aoao": # type: ignore
        print(f"Search for items with attributes {attributes}")
        unlocked = []
        locked = []
        for collection in self.collections.values():
            if collection.locked:
                locked.extend(collection.search_items(attributes))
            else:
                unlocked.extend(collection.search_items(attributes))
        print(f"Found '{len(unlocked)}' unlocked and '{len(locked)}' locked items")
        return [unlocked, locked]

    @method()
    def Unlock(self, objects: "ao") -> "aoo": # type: ignore
        prompt = "/"
        return [objects, prompt]

    @method()
    def Lock(self, objects: "ao") -> "aoo": # type: ignore
        prompt = "/"
        return [objects, prompt]

    @method()
    def GetSecrets(self, items: "ao", session: "o") -> "a{o(oayays)}": # type: ignore
        items: list[Item|None] = [self.path_to_item(item) for item in items]
        return {item.path: list(item.get_secret(session)) for item in items if item != None}

    @method()
    def ReadAlias(self, alias: "s") -> "o": # type: ignore
        id = self.aliases.get(alias)
        return self.collections[id].path if id else "/"

    @method()
    def SetAlias(self, name: "s", collection: "o") -> "": # type: ignore
        if collection == "/":
            if name in self.aliases:
                self.store.del_collection_alias(name)
                self.unexport_alias(name, self.aliases[name])
                self.aliases.pop(name)
            return # type: ignore

        parts = self.path_parts(collection)
        if len(parts) != 2 or parts[0] != "collection":
            raise errors.NoSuchObject(collection)
        id = parts[1]
        self.store.set_collection_alias(name, id)
        self.export_alias(name, id)
        self.aliases[name] = id

    @signal()
    def CollectionCreated(self, collection: Collection) -> "o": # type: ignore
        return collection.path

    @signal()
    def CollectionDeleted(self, collection: Collection) -> "o": # type: ignore
        return collection.path

    @signal()
    def CollectionChanged(self, collection: Collection) -> "o": # type: ignore
        return collection.path

    @dbus_property(PropertyAccess.READ)
    def Collections(self) -> "ao": # type: ignore
        return [collection.path for collection in self.collections.values()]
