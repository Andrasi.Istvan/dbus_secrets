from dbus_next.errors import DBusError

class IsLocked(DBusError):
    def __init__(self, *args, **kwargs): super().__init__("org.freedesktop.Secret.Error.IsLocked", *args, **kwargs)

class NoSession(DBusError):
    def __init__(self, *args, **kwargs): super().__init__("org.freedesktop.Secret.Error.NoSession", *args, **kwargs)

class NoSuchObject(DBusError):
    def __init__(self, *args, **kwargs): super().__init__("org.freedesktop.Secret.Error.NoSuchObject", *args, **kwargs)
