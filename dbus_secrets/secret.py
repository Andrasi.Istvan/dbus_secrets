from typing import NamedTuple

class Secret(NamedTuple):
    session:      str
    parameters:   bytes
    value:        bytes
    content_type: str
