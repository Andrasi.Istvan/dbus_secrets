from typing import TYPE_CHECKING
from dbus_next.signature import Variant
from dbus_next.constants import PropertyAccess
from dbus_next.service import ServiceInterface, method, signal, dbus_property

from . import errors
from .item import Item
from .secret import Secret
if TYPE_CHECKING:
    from .store import ID, LABEL, ATTRIBUTES
    from .service import Service

class Collection(ServiceInterface):

    LABEL = "org.freedesktop.Secret.Collection.Label"

    def __init__(self, service: "Service", id: "ID"):
        super().__init__("org.freedesktop.Secret.Collection")
        self.service = service

        self.bus   = service.bus
        self.path  = service.path + "/collection/" + id
        self.store = service.store

        self.id     = id
        self.locked = False
        self.loaded = False
        self.items: dict[ID, Item] = {}

    def init(self) -> "Collection":
        if self.loaded:
            return self

        self.loaded = True
        items = self.store.get_items_for_collection(self.id)
        print(f"Initializing {self} with {len(items)} items(s)")
        for id in items:
            self.items[id] = Item(self, id)
        return self

    def __str__(self) -> str:
        return f"<Collection '{self.Label}' ({self.id})>"

    def export(self):
        self.bus.export(self.path, self)
        self.export_items()

    def export_items(self):
        for item in self.items.values():
            item.export()

    def unexport(self):
        self.bus.unexport(self.path, self)
        self.unexport_items()

    def unexport_items(self):
        for item in self.items.values():
            item.unexport()

    @method()
    def Delete(self) -> "o": # type: ignore
        prompt = "/" # No prompt

        for item in self.items.values():
            p = item.Delete()
            if p != "/": return p

        self.store.delete_collection(self.id)

        for alias in [alias for alias, id in self.service.aliases.items() if id == self.id]:
            self.service.unexport_alias(alias, self.id)
            self.service.aliases.pop(alias)

        self.unexport()
        self.service.collections.pop(self.id)
        self.service.CollectionDeleted(self)

        return prompt

    # TODO remove after dbus_next @method() decorator is fixed, so SearchItems can be used directly.
    # https://github.com/altdesktop/python-dbus-next/pull/123
    def search_items(self, attributes: "a{ss}") -> "ao": # type: ignore
        print(f"Search for items in {self} with attributes {attributes}")
        ids = self.store.get_items_for_attributes(self.id, attributes)
        return [item.path for id, item in self.items.items() if id in ids]

    @method()
    def SearchItems(self, attributes: "a{ss}") -> "ao": # type: ignore
        return self.search_items(attributes)

    @method()
    def CreateItem(self, properties: "a{sv}", secret: "(oayays)", replace: "b") -> "oo": # type: ignore
        prompt = "/"

        secret: Secret = Secret(*secret)
        session = self.service.path_to_session(secret.session)
        if session is None:
            raise errors.NoSession(secret.session)

        data       = session.decode_secret(secret)
        properties : dict[str, Variant] = properties
        label      : LABEL      = properties.get(Item.LABEL,      Variant("s",     "")).value
        attributes : ATTRIBUTES = properties.get(Item.ATTRIBUTES, Variant("a{ss}", {})).value

        print(f"Create item '{label}' with attributes '{attributes}'")
        id = None
        if replace:
            ids = self.store.get_items_for_attributes(self.id, attributes)
            if len(ids) > 0:
                id = list(ids)[0]
        if id is None:
            id = self.store.create_item(self.id, data, label, attributes)

        item = Item(self, id)
        item.export()
        print(f"Exported {item} at '{item.path}'")
        self.items[id] = item
        self.ItemCreated(item)
        return [item.path, prompt]

    @signal()
    def ItemCreated(self, item: Item) -> "o": # type: ignore
        return item.path

    @signal()
    def ItemDeleted(self, item: Item) -> "o": # type: ignore
        return item.path

    @signal()
    def ItemChanged(self, item: Item) -> "o": # type: ignore
        return item.path

    @dbus_property(PropertyAccess.READ)
    def Items(self) -> "ao": # type: ignore
        return [item.path for item in self.items.values()]

    @dbus_property(PropertyAccess.READWRITE)
    def Label(self) -> "s": # type: ignore
        return self.store.get_collection_label(self.id)

    @Label.setter
    def Label(self, label: "s"): # type: ignore
        if self.Label != label:
            self.store.set_collection_label(self.id, label)
            self.emit_properties_changed({"Label": label})
            self.service.CollectionChanged(self)

    @dbus_property(PropertyAccess.READ)
    def Locked(self) -> "b": # type: ignore
        return self.locked

    @dbus_property(PropertyAccess.READ)
    def Created(self) -> "t": # type: ignore
        return 0

    @dbus_property(PropertyAccess.READ)
    def Modified(self) -> "t": # type: ignore
        return 0
