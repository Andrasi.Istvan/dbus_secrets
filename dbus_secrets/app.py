from dbus_next.signature import Variant
from dbus_next.constants import RequestNameReply, ReleaseNameReply
from dbus_next.aio.message_bus import MessageBus

from .store import Store
from .collection import Collection
from .service import Service

class App:

    def __init__(self, store: Store, bus: MessageBus, bus_name: str = "org.freedesktop.secrets"):
        self.store    = store
        self.bus      = bus
        self.bus_name = bus_name
        self.service  = None

    async def __aenter__(self) -> "App":
        await self.bus.connect()
        print("Connected to dbus instance")
        response = await self.bus.request_name(self.bus_name)
        if response != RequestNameReply.PRIMARY_OWNER:
            raise RuntimeError(f"Failed to request dbus name: '{self.bus_name}'")
        print(f"Service name '{self.bus_name}' acquired")
        self.service = Service(self.bus, self.store).init()
        self.service.export()
        print(f"Exported {self.service} at '{self.service.path}'")
        self.service.CreateCollection({Collection.LABEL: Variant("s", "DBusSecrets")}, "default")
        return self

    async def __aexit__(self, _et, _ev, _eb):
        if self.service is not None:
            self.service.unexport()
            self.service = None

        response = await self.bus.release_name(self.bus_name)
        if response != ReleaseNameReply.RELEASED:
            print(f"Failed to release dbus name: '{self.bus_name}'")

        self.bus.disconnect()
