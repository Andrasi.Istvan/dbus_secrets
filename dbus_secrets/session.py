from typing import TYPE_CHECKING, Optional
from dbus_next.errors import DBusError
from dbus_next.constants import ErrorType
from dbus_next.service import ServiceInterface, method

import os

from hmac import HMAC
from hashlib import sha256

from Cryptodome.Util.Padding import pad, unpad
from Cryptodome.Cipher import AES

from .secret import Secret
if TYPE_CHECKING:
    from .store import ID
    from .service import Service

DH_PRIME = int("""
FFFFFFFF FFFFFFFF C90FDAA2 2168C234 C4C6628B 80DC1CD1 29024E08 8A67CC74
020BBEA6 3B139B22 514A0879 8E3404DD EF9519B3 CD3A431B 302B0A6D F25F1437
4FE1356D 6D51C245 E485B576 625E7EC6 F44C42E9 A637ED6B 0BFF5CB6 F406B7ED
EE386BFB 5A899FA5 AE9F2411 7C4B1FE6 49286651 ECE65381 FFFFFFFF FFFFFFFF
""".replace(" ", "").replace("\n", ""), 16)

class Session(ServiceInterface):

    def __init__(self, service: "Service", id: "ID", algorithm: str, input: bytes):
        super().__init__("org.freedesktop.Secret.Session")
        self.service = service

        self.id   = id
        self.bus  = service.bus
        self.path = service.path + "/session/" + id
        self.algorithm = algorithm

        self.pub_key: Optional[bytes] = None
        self.aes_key: Optional[bytes] = None

        if algorithm == "plain":
            pass
        elif algorithm == "dh-ietf1024-sha256-aes128-cbc-pkcs7":
            private_key, self.pub_key = new_keypair()
            self.aes_key = derive_aes_key(private_key, input)
        else:
            raise DBusError(ErrorType.NOT_SUPPORTED, f'session algorithm "{algorithm}" is not supported')

    def __str__(self) -> str:
        return f"<Session '{self.algorithm}' ({self.id})>"

    def encode_secret(self, data: bytes) -> Secret:
        aes_iv = b""
        if self.aes_key is not None:
            aes_iv, data = encode(self.aes_key, data)
        return Secret(self.path, aes_iv, data, "application/octet-stream")

    def decode_secret(self, secret: Secret) -> bytes:
        if secret.session != self.path:
            raise RuntimeError("invalid session when decoding secret")

        if secret.content_type != "application/octet-stream":
            print(f"Secret data content type is '{secret.content_type}', not 'application/octet-stream'!")

        data = secret.value
        if self.aes_key is not None:
            data = decode(self.aes_key, secret.parameters, data)

        return data

    def export(self):
        self.bus.export(self.path, self)

    def unexport(self):
        self.bus.unexport(self.path, self)

    @method()
    def Close(self):
        self.unexport()
        self.service.sessions.pop(self.id)


def new_keypair() -> tuple[bytes, bytes]:
    private_key = os.urandom(0x80)
    public_key = pow(2, int.from_bytes(private_key, "big"), DH_PRIME).to_bytes(0x80, "big")
    return private_key, public_key

def derive_aes_key(private_key: bytes, public_key: bytes):
    salt = b"\x00" * 0x20
    shared_secret = pow(int.from_bytes(public_key, "big"), int.from_bytes(private_key, "big"), DH_PRIME)

    hmac = HMAC(salt, digestmod=sha256)
    hmac.update(shared_secret.to_bytes(0x80, "big"))
    shared_key = hmac.digest()

    hmac = HMAC(shared_key, digestmod=sha256)
    hmac.update(b"\x01")
    aes_key = hmac.digest()[:0x10]

    return aes_key

def encode(aes_key: bytes, data: bytes) -> tuple[bytes, bytes]:
    aes_iv = os.urandom(0x10)
    cipher = AES.new(aes_key, AES.MODE_CBC, aes_iv)
    data = pad(data, 0x80)
    data = cipher.encrypt(data)
    return aes_iv, data

def decode(aes_key: bytes, aes_iv: bytes, data: bytes) -> bytes:
    cipher = AES.new(aes_key, AES.MODE_CBC, aes_iv)
    data = cipher.decrypt(data)
    data = unpad(data, 0x80)
    return data
