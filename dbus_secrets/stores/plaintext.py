from typing import Optional

import uuid
import json
import shutil

from pathlib import Path

from ..store import ID, LABEL, ALIAS, ATTRIBUTES

class Store:

    def __init__(self, dir: Path):
        self.path = dir
        (self.path / '.aliases').mkdir(parents=True, exist_ok=True)
        self.path.chmod(0o700)

    def get_collections(self) -> dict[ID, set[ALIAS]]:
        aliases = {path.name: path.readlink().name for path in (self.path / '.aliases').iterdir() if path.is_symlink()}
        collections = [path.name for path in self.path.iterdir() if path.is_dir() and not str(path.name).startswith('.')]
        return {c: set(alias for alias, a in aliases.items() if a == c) for c in collections}

    def get_collection_label(self, c: ID) -> LABEL:
        with open(self.path / c / ".label", "r") as f:
            return f.read()

    def set_collection_label(self, c: ID, l: LABEL) -> None:
        with open(self.path / c / ".label", "w") as f:
            f.write(l)

    def set_collection_alias(self, a: ALIAS, c: ID) -> None:        (self.path / ".aliases" / a).symlink_to(Path("..") / c, target_is_directory=True)
    def del_collection_alias(self, a: ALIAS) -> None:               (self.path / ".aliases" / a).unlink()
    def get_collection_for_alias(self, a: "ALIAS") -> Optional[ID]: return (self.path / ".aliases" / a).readlink().name
    def delete_collection(self, c: ID) -> None:                     shutil.rmtree(self.path / c)
    def delete_item(self, c: ID, i: ID) -> None:                    shutil.rmtree(self.path / c / i)

    def set_item_label(self, c: ID, i: ID, l: LABEL) -> None:
        with open(self.path / c / i / ".label", "w") as f:
            f.write(l)

    def set_item_secret(self, c: ID, i: ID, secret: bytes) -> None:
        with open(self.path / c / i / ".secret", "wb") as f:
            f.write(secret)

    def set_item_attributes(self, c: ID, i: ID, a: ATTRIBUTES) -> None:
        with open(self.path / c / i / ".attributes", "w") as f:
            json.dump(a, f)

    def get_item_label(self, c: ID, i: ID) -> LABEL:
        with open(self.path / c / i / ".label", "r") as f:
            return f.read()

    def get_item_secret(self, c: ID, i: ID) -> bytes:
        with open(self.path / c / i / ".secret", "rb") as f:
            return f.read()

    def get_item_attributes(self, c: ID, i: ID) -> ATTRIBUTES:
        with open(self.path / c / i / ".attributes", "r") as f:
            return json.load(f)

    def get_items_for_collection(self, c: ID) -> set[ID]:                return set(path.name for path in (self.path / c).iterdir() if path.is_dir())
    def get_items_for_attributes(self, c: ID, a: ATTRIBUTES) -> set[ID]: return set(id for id in self.get_items_for_collection(c) if matches(self.get_item_attributes(c, id), a))

    def create_collection(self, l: LABEL, a: set[ALIAS]) -> ID:
        id = uuid.uuid4().hex
        (self.path / id).mkdir(parents=True)
        self.set_collection_label(id, l)
        for alias in a:
            self.set_collection_alias(alias, id)
        return id

    def create_item(self, c: ID, secret: bytes, l: LABEL, a: ATTRIBUTES) -> ID:
        id = uuid.uuid4().hex
        (self.path / c / id).mkdir(parents=True)
        self.set_item_label(c, id, l)
        self.set_item_secret(c, id, secret)
        self.set_item_attributes(c, id, a)
        return id

def matches(a1: ATTRIBUTES, a2: ATTRIBUTES) -> bool:
    for k, v in a2.items():
        if a1.get(k) != v:
            return False
    return True
