from typing import Optional
from dataclasses import dataclass

import uuid

from ..store import ID, LABEL, ALIAS, ATTRIBUTES

class Store:

    def __init__(self):
        self.aliases     : dict[ALIAS, Optional[ID]] = {}
        self.collections : dict[ID, Collection]      = {}

    def get_collections(self) -> dict[ID, set[ALIAS]]:                   return {c: set(alias for alias, a in self.aliases.items() if a == c) for c in self.collections.keys()}
    def get_collection_label(self, c: ID) -> LABEL:                      return self.collections[c].label
    def set_collection_label(self, c: ID, l: LABEL) -> None:             self.collections[c].label = l
    def set_collection_alias(self, a: ALIAS, c: ID) -> None:             self.aliases[a] = c if self.collections[c] else None
    def del_collection_alias(self, a: ALIAS) -> None:                    self.aliases.pop(a)
    def get_collection_for_alias(self, a: "ALIAS") -> Optional[ID]:      return self.aliases.get(a)
    def delete_collection(self, c: ID) -> None:                          self.collections.pop(c)
    def delete_item(self, c: ID, i: ID) -> None:                         self.collections[c].items.pop(i)
    def set_item_label(self, c: ID, i: ID, l: LABEL) -> None:            self.collections[c].items[i].label = l
    def set_item_secret(self, c: ID, i: ID, secret: bytes) -> None:      self.collections[c].items[i].secret = secret
    def set_item_attributes(self, c: ID, i: ID, a: ATTRIBUTES) -> None:  self.collections[c].items[i].attributes = a
    def get_item_label(self, c: ID, i: ID) -> LABEL:                     return self.collections[c].items[i].label
    def get_item_secret(self, c: ID, i: ID) -> bytes:                    return self.collections[c].items[i].secret
    def get_item_attributes(self, c: ID, i: ID) -> ATTRIBUTES:           return self.collections[c].items[i].attributes
    def get_items_for_collection(self, c: ID) -> set[ID]:                return set(self.collections[c].items.keys())
    def get_items_for_attributes(self, c: ID, a: ATTRIBUTES) -> set[ID]: return set(id for id, item in self.collections[c].items.items() if item.matches(a))

    def create_collection(self, l: LABEL, a: set[ALIAS]) -> ID:
        id = uuid.uuid4().hex
        self.collections[id] = Collection(l, {})
        for alias in a:
            self.aliases[alias] = id
        return id

    def create_item(self, c: ID, secret: bytes, l: LABEL, a: ATTRIBUTES) -> ID:
        id = uuid.uuid4().hex
        self.collections[c].items[id] = Item(l, a, secret)
        return id

@dataclass
class Collection:
    label: LABEL
    items: dict[ID, "Item"]

@dataclass
class Item:
    label: LABEL
    attributes: ATTRIBUTES
    secret: bytes

    def matches(self, a: ATTRIBUTES) -> bool:
        for k, v in a.items():
            if self.attributes.get(k) != v:
                return False
        return True
