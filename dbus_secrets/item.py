from typing import TYPE_CHECKING
from dbus_next.constants import PropertyAccess
from dbus_next.service import ServiceInterface, method, dbus_property

from . import errors
from .secret import Secret
if TYPE_CHECKING:
    from .store import ID
    from .collection import Collection

class Item(ServiceInterface):

    LABEL      = "org.freedesktop.Secret.Item.Label"
    ATTRIBUTES = "org.freedesktop.Secret.Item.Attributes"

    def __init__(self, collection: "Collection", id: "ID"):
        super().__init__("org.freedesktop.Secret.Item")
        self.collection = collection

        self.id    = id
        self.bus   = collection.bus
        self.path  = collection.path + "/" + id
        self.store = collection.store

    def __str__(self) -> str:
        return f"<Item '{self.Label}' ({self.id})>"

    def export(self):
        self.bus.export(self.path, self)

    def unexport(self):
        self.bus.unexport(self.path, self)

    @method()
    def Delete(self) -> "o": # type: ignore
        prompt = "/" # No prompt

        print(f"Delete {self}")
        self.store.delete_item(self.collection.id, self.id)

        self.unexport()
        self.collection.items.pop(self.id)
        self.collection.ItemDeleted(self)

        return prompt

    # TODO remove after dbus_next @method() decorator is fixed, so GetSecret can be used directly.
    # https://github.com/altdesktop/python-dbus-next/pull/123
    def get_secret(self, session: str) -> Secret:
        _session = self.collection.service.path_to_session(session)
        if _session is None:
            raise errors.NoSession(session)

        print(f"Get secret for {self} trough {session}")
        data = self.store.get_item_secret(self.collection.id, self.id)
        return _session.encode_secret(data)

    @method()
    def GetSecret(self, session: "o") -> "(oayays)": # type: ignore
        return list(self.get_secret(session))

    @method()
    def SetSecret(self, secret: "(oayays)"): # type: ignore
        secret: Secret = Secret(*secret)
        session = self.collection.service.path_to_session(secret.session)
        if session is None:
            raise errors.NoSession(secret.session)

        print(f"Set secret for {self} trough {session}")
        data = session.decode_secret(secret)
        self.store.set_item_secret(self.collection.id, self.id, data)
        self.collection.ItemChanged(self)

    @dbus_property(access = PropertyAccess.READ)
    def Locked(self) -> "b": # type: ignore
        return False

    @dbus_property(access = PropertyAccess.READWRITE)
    def Attributes(self) -> "a{ss}": # type: ignore
        return self.store.get_item_attributes(self.collection.id, self.id)

    @Attributes.setter
    def Attributes(self, attributes: "a{ss}"): # type: ignore
        if self.Attributes != attributes:
            self.store.set_item_attributes(self.collection.id, self.id, attributes)
            self.emit_properties_changed({"Attributes": attributes})
            self.collection.ItemChanged(self)

    @dbus_property(access = PropertyAccess.READWRITE)
    def Label(self) -> "s": # type: ignore
        return self.store.get_item_label(self.collection.id, self.id)

    @Label.setter
    def Label(self, label: "s"): # type: ignore
        if self.Label != label:
            self.store.set_item_label(self.collection.id, self.id, label)
            self.emit_properties_changed({"Label": label})
            self.collection.ItemChanged(self)

    @dbus_property(access = PropertyAccess.READ)
    def Created(self) -> "t": # type: ignore
        return 0

    @dbus_property(access = PropertyAccess.READ)
    def Modified(self) -> "t": # type: ignore
        return 0
