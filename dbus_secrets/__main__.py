import os
import signal
import asyncio

from pathlib import Path
from dbus_next.aio.message_bus import MessageBus

from .app import App
from .stores import inmem, plaintext

class MessageBusException(Exception):
    pass

async def run():
    try:
        bus = MessageBus()
    except Exception as e:
        raise MessageBusException(str(e)) from e

    store = None
    store_type = os.getenv("STORE", "inmem")
    if store_type == "inmem":
        store = inmem.Store()
        print("Using 'inmem' storage.")
    elif store_type == "plaintext":
        store_path = Path(os.getenv("XDG_CACHE_HOME", Path.home() / ".cache"))
        store = plaintext.Store(store_path / "dbus_secrets")
        print("Using 'plaintext' storage at '", store.path,"'", sep="")
    if store is None:
        raise RuntimeError(f"invalid store type: '{store_type}'")

    async with App(store, bus) as app:
        await app.bus.wait_for_disconnect()
        print("Bus disconnected. Exiting...")

async def wrap():
    task = asyncio.create_task(run())
    loop = asyncio.get_event_loop()
    loop.add_signal_handler(signal.SIGTERM, task.cancel)
    loop.add_signal_handler(signal.SIGINT,  task.cancel)
    await task

def main():
    try:
        asyncio.run(wrap())
    except asyncio.exceptions.CancelledError:
        print("\nCanceled.")
    except RuntimeError as e:
        print("RuntimeError:", e)
    except MessageBusException as e:
        print("MessageBus:", e)
