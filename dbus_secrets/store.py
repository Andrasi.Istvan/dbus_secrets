from typing import TypeAlias, Protocol, Optional

ID: TypeAlias = str
LABEL: TypeAlias = str
ALIAS: TypeAlias = str
ATTRIBUTES: TypeAlias = dict[str, str]

class Store(Protocol):
    def create_collection(self, l: LABEL, a: set[ALIAS]) -> ID:                 pass # type: ignore
    def delete_collection(self, c: ID) -> None:                                 pass # type: ignore
    def get_collections(self) -> dict[ID, set[ALIAS]]:                          pass # type: ignore
    def get_collection_label(self, c: ID) -> LABEL:                             pass # type: ignore
    def set_collection_label(self, c: ID, l: LABEL) -> None:                    pass # type: ignore
    def set_collection_alias(self, a: ALIAS, c: ID) -> None:                    pass # type: ignore
    def del_collection_alias(self, a: ALIAS) -> None:                           pass # type: ignore
    def get_collection_for_alias(self, a: ALIAS) -> Optional[ID]:               pass # type: ignore
    def get_items_for_collection(self, c: ID) -> set[ID]:                       pass # type: ignore
    def get_items_for_attributes(self, c: ID, a: ATTRIBUTES) -> set[ID]:        pass # type: ignore
    def create_item(self, c: ID, secret: bytes, l: LABEL, a: ATTRIBUTES) -> ID: pass # type: ignore
    def delete_item(self, c: ID, i: ID) -> None:                                pass # type: ignore
    def get_item_label(self, c: ID, i: ID) -> LABEL:                            pass # type: ignore
    def set_item_label(self, c: ID, i: ID, l: LABEL) -> None:                   pass # type: ignore
    def get_item_attributes(self, c: ID, i: ID) -> ATTRIBUTES:                  pass # type: ignore
    def set_item_attributes(self, c: ID, i: ID, a: ATTRIBUTES) -> None:         pass # type: ignore
    def get_item_secret(self, c: ID, i: ID) -> bytes:                           pass # type: ignore
    def set_item_secret(self, c: ID, i: ID, secret: bytes) -> None:             pass # type: ignore
