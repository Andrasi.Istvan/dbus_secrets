# dbus_secrets

[DBus Secret Service](https://specifications.freedesktop.org/secret-service/) implementation.

## Build
~~~
poetry install
pytest
~~~

## Install
~~~
pipx install .
# or
cp dist/dbus_secrets "$XDG_BIN_DIR/"
~~~

The cffi crypto backend library is inside, but it needs the system python3-cffi package somehow.

## Run
~~~
STORE=plaintext dbus_secrets
~~~
