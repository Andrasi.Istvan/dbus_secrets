import pytest
import os

from dbus_next.errors import DBusError, InterfaceNotFoundError
from dbus_next.signature import Variant
from dbus_next.aio.proxy_object import ProxyInterface

from dbus_secrets.session import new_keypair, derive_aes_key, encode, decode

from conftest import MessageBusProxy

pytestmark = pytest.mark.asyncio

@pytest.mark.usefixtures('app')
async def test_property_collections(service_proxy: ProxyInterface):
    await service_proxy.get_collections() # type: ignore
    with pytest.raises(DBusError):
        await service_proxy.set_collections([]) # type: ignore

@pytest.mark.usefixtures('app')
async def test_dafault_alias(service_proxy: ProxyInterface):
    assert await service_proxy.call_read_alias("default") in await service_proxy.get_collections() # type: ignore

@pytest.mark.usefixtures('app')
async def test_manage_collection(bus: MessageBusProxy, service_proxy: ProxyInterface):
    on_created = []
    on_changed = []
    on_deleted = []
    service_proxy.on_collection_created(lambda c: on_created.append(c)) # type: ignore
    service_proxy.on_collection_changed(lambda c: on_changed.append(c)) # type: ignore
    service_proxy.on_collection_deleted(lambda c: on_deleted.append(c)) # type: ignore

    collection_alias = "TC"
    collection_path, _ = await service_proxy.call_create_collection({"org.freedesktop.Secret.Collection.Label": Variant("s", "TestCollection")}, collection_alias) # type: ignore

    assert on_created == [collection_path]
    assert collection_path in await service_proxy.get_collections() # type: ignore
    assert await service_proxy.call_read_alias(collection_alias) == collection_path # type: ignore

    collection_alias_extra = "TCE"
    assert await service_proxy.call_read_alias(collection_alias_extra) == "/" # type: ignore
    await service_proxy.call_set_alias(collection_alias_extra, collection_path) # type: ignore
    assert await service_proxy.call_read_alias(collection_alias_extra) == collection_path # type: ignore
    await service_proxy.call_set_alias(collection_alias_extra, "/") # type: ignore
    assert await service_proxy.call_read_alias(collection_alias_extra) == "/" # type: ignore

    collection_proxy = await bus.collection(collection_path)

    await collection_proxy.set_label("NewLabel") # type: ignore
    assert on_changed == [collection_path]

    await collection_proxy.call_delete() # type: ignore
    assert on_deleted == [collection_path]
    assert collection_path not in await service_proxy.get_collections() # type: ignore
    assert await service_proxy.call_read_alias(collection_alias) == "/" # type: ignore

test_manage_session_args = [
    ("plain", b""),
    ("dh-ietf1024-sha256-aes128-cbc-pkcs7", os.urandom(16)),
]
@pytest.mark.usefixtures('app')
@pytest.mark.parametrize(("algorithm", "parameter"), test_manage_session_args, ids = [a[0] for a in test_manage_session_args])
async def test_manage_session(algorithm: str, parameter: bytes, bus: MessageBusProxy, service_proxy: ProxyInterface):
    _, session_path = await service_proxy.call_open_session(algorithm, Variant("ay", parameter)) # type: ignore

    session_proxy = await bus.session(session_path)

    await session_proxy.call_close() # type: ignore

    with pytest.raises(InterfaceNotFoundError):
        await bus.session(session_path)

@pytest.mark.usefixtures('app')
async def test_manage_secrets(service_proxy: ProxyInterface, session_proxy: ProxyInterface, collection_proxy: ProxyInterface):
    item_data_1 = b"TestData1"
    item_path_1, _ = await collection_proxy.call_create_item({}, # type: ignore
        [session_proxy.path, b"", item_data_1, "application/octet-stream"], False)

    item_data_2 = b"TestData2"
    item_path_2, _ = await collection_proxy.call_create_item({}, # type: ignore
        [session_proxy.path, b"", item_data_2, "application/octet-stream"], False)

    results = await service_proxy.call_get_secrets([item_path_1, item_path_2], session_proxy.path) # type: ignore

    assert results == {
        item_path_1: [session_proxy.path, b"", item_data_1, "application/octet-stream"],
        item_path_2: [session_proxy.path, b"", item_data_2, "application/octet-stream"],
    }

@pytest.mark.usefixtures('app')
async def test_manage_secrets_encrypted(bus: MessageBusProxy, service_proxy: ProxyInterface, collection_proxy: ProxyInterface):
    key, input = new_keypair()
    pub_key, path = await service_proxy.call_open_session("dh-ietf1024-sha256-aes128-cbc-pkcs7", Variant("ay", input)) # type: ignore
    session_proxy = await bus.session(path)

    aes_key = derive_aes_key(key, pub_key.value)

    item_data_1 = b"TestData1"
    item_path_1, _ = await collection_proxy.call_create_item({}, # type: ignore
        [session_proxy.path, *encode(aes_key, item_data_1), "application/octet-stream"], False)

    item_data_2 = b"TestData2"
    item_path_2, _ = await collection_proxy.call_create_item({}, # type: ignore
        [session_proxy.path, *encode(aes_key, item_data_2), "application/octet-stream"], False)

    service_proxy = await bus.service()
    results = await service_proxy.call_get_secrets([item_path_1, item_path_2], session_proxy.path) # type: ignore

    path, iv, data, mime = results[item_path_1]
    assert path == session_proxy.path
    assert mime == "application/octet-stream"
    assert decode(aes_key, iv, data) == item_data_1

    path, iv, data, mime = results[item_path_2]
    assert path == session_proxy.path
    assert mime == "application/octet-stream"
    assert decode(aes_key, iv, data) == item_data_2

@pytest.mark.usefixtures('app')
async def test_search_items(bus: MessageBusProxy, session_proxy: ProxyInterface, collection_proxy: ProxyInterface):
    item_path_1, _ = await collection_proxy.call_create_item({ # type: ignore
        "org.freedesktop.Secret.Item.Label": Variant("s", "TestLabel1"),
        "org.freedesktop.Secret.Item.Attributes": Variant("a{ss}", { "k1": "v1" }),
    }, [session_proxy.path, b"", b"TestData1", "application/octet-stream"], False)
    item_path_2, _ = await collection_proxy.call_create_item({ # type: ignore
        "org.freedesktop.Secret.Item.Label": Variant("s", "TestLabel2"),
        "org.freedesktop.Secret.Item.Attributes": Variant("a{ss}", { "k2": "v2" }),
    }, [session_proxy.path, b"", b"TestData2", "application/octet-stream"], False)
    item_path_3, _ = await collection_proxy.call_create_item({ # type: ignore
        "org.freedesktop.Secret.Item.Label": Variant("s", "TestLabel3"),
        "org.freedesktop.Secret.Item.Attributes": Variant("a{ss}", { "k1": "v1", "k2": "v2" }),
    }, [session_proxy.path, b"", b"TestData3", "application/octet-stream"], False)

    service_proxy = await bus.service()
    assert await service_proxy.call_search_items({"k1": "v1"}) == [[item_path_1, item_path_3], []] # type: ignore
    assert await service_proxy.call_search_items({"k2": "v2"}) == [[item_path_2, item_path_3], []] # type: ignore
    assert await service_proxy.call_search_items({"k1": "v1", "k2": "v2"}) ==  [[item_path_3], []] # type: ignore
