import pytest
import os
import uuid

from dbus_next.errors import DBusError
from dbus_next.signature import Variant
from dbus_next.aio.proxy_object import ProxyInterface

from dbus_secrets.session import new_keypair, derive_aes_key, encode, decode

from conftest import MessageBusProxy

pytestmark = pytest.mark.asyncio

@pytest.mark.usefixtures('app')
async def test_property_locked(item_proxy: ProxyInterface):
    await item_proxy.get_locked() # type: ignore
    with pytest.raises(DBusError):
        await item_proxy.set_locked(True) # type: ignore

@pytest.mark.usefixtures('app')
async def test_property_created(item_proxy: ProxyInterface):
    await item_proxy.get_created() # type: ignore
    with pytest.raises(DBusError):
        await item_proxy.set_created(1) # type: ignore

@pytest.mark.usefixtures('app')
async def test_property_modified(item_proxy: ProxyInterface):
    await item_proxy.get_modified() # type: ignore
    with pytest.raises(DBusError):
        await item_proxy.set_modified(1) # type: ignore

@pytest.mark.usefixtures('app')
async def test_property_label(item_proxy: ProxyInterface, item_label: str):
    assert await item_proxy.get_label() == item_label # type: ignore
    item_label_new = uuid.uuid4().hex
    await item_proxy.set_label(item_label_new) # type: ignore
    assert await item_proxy.get_label() == item_label_new # type: ignore

@pytest.mark.usefixtures('app')
async def test_property_attributes(item_proxy: ProxyInterface, item_attributes: dict[str, str]):
    assert await item_proxy.get_attributes() == item_attributes # type: ignore
    item_attributes_new = { "test_data": uuid.uuid4().hex }
    await item_proxy.set_attributes(item_attributes_new) # type: ignore
    assert await item_proxy.get_attributes() == item_attributes_new # type: ignore

@pytest.mark.usefixtures('app')
async def test_manage_secret(item_proxy: ProxyInterface, item_data: bytes, session_proxy: ProxyInterface):
    assert await item_proxy.call_get_secret(session_proxy.path) == [session_proxy.path, b"", item_data, "application/octet-stream"] # type: ignore

    item_data_new = os.urandom(260)
    await item_proxy.call_set_secret([session_proxy.path, b"", item_data_new, "application/octet-stream"]) # type: ignore
    assert await item_proxy.call_get_secret(session_proxy.path) == [session_proxy.path, b"", item_data_new, "application/octet-stream"] # type: ignore

@pytest.mark.usefixtures('app')
async def test_manage_secret_encrypted(bus: MessageBusProxy, collection_proxy: ProxyInterface):
    key, input = new_keypair()
    service_proxy = await bus.service()
    pub_key, path = await service_proxy.call_open_session("dh-ietf1024-sha256-aes128-cbc-pkcs7", Variant("ay", input)) # type: ignore
    session_proxy = await bus.session(path)

    item_data = uuid.uuid4().hex.encode("UTF-8")

    aes_key = derive_aes_key(key, pub_key.value)
    iv, data = encode(aes_key, item_data)

    item_path, _ = await collection_proxy.call_create_item({ # type: ignore
        "org.freedesktop.Secret.Item.Label": Variant("s", "TestItem"),
        "org.freedesktop.Secret.Item.Attributes": Variant("a{ss}", { "test_data": uuid.uuid4().hex }),
    }, [session_proxy.path, iv, data, "application/octet-stream"], False)

    item_proxy = await bus.item(item_path)
    path, iv, data, mime = await item_proxy.call_get_secret(session_proxy.path) # type: ignore
    assert path == session_proxy.path
    assert mime == "application/octet-stream"
    assert decode(aes_key, iv, data) == item_data
