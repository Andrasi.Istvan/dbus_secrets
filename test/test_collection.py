import pytest
import uuid

from dbus_next.errors import DBusError
from dbus_next.signature import Variant
from dbus_next.aio.proxy_object import ProxyInterface

from conftest import MessageBusProxy

pytestmark = pytest.mark.asyncio

@pytest.mark.usefixtures('app')
async def test_property_items(collection_proxy: ProxyInterface):
    await collection_proxy.get_items() # type: ignore
    with pytest.raises(DBusError):
        await collection_proxy.set_items([]) # type: ignore

@pytest.mark.usefixtures('app')
async def test_property_locked(collection_proxy: ProxyInterface):
    await collection_proxy.get_locked() # type: ignore
    with pytest.raises(DBusError):
        await collection_proxy.set_locked(True) # type: ignore

@pytest.mark.usefixtures('app')
async def test_property_created(collection_proxy: ProxyInterface):
    await collection_proxy.get_created() # type: ignore
    with pytest.raises(DBusError):
        await collection_proxy.set_created(1) # type: ignore

@pytest.mark.usefixtures('app')
async def test_property_modified(collection_proxy: ProxyInterface):
    await collection_proxy.get_modified() # type: ignore
    with pytest.raises(DBusError):
        await collection_proxy.set_modified(1) # type: ignore

@pytest.mark.usefixtures('app')
async def test_property_label(collection_proxy: ProxyInterface, collection_label: str):
    assert await collection_proxy.get_label() == collection_label # type: ignore
    collection_label_new = uuid.uuid4().hex
    await collection_proxy.set_label(collection_label_new) # type: ignore
    assert await collection_proxy.get_label() == collection_label_new # type: ignore

@pytest.mark.usefixtures('app')
async def test_manage_item(bus: MessageBusProxy, session_proxy: ProxyInterface, collection_proxy: ProxyInterface):
    on_created = []
    on_changed = []
    on_deleted = []
    collection_proxy.on_item_created(lambda c: on_created.append(c)) # type: ignore
    collection_proxy.on_item_changed(lambda c: on_changed.append(c)) # type: ignore
    collection_proxy.on_item_deleted(lambda c: on_deleted.append(c)) # type: ignore

    item_path, _ = await collection_proxy.call_create_item({ # type: ignore
        "org.freedesktop.Secret.Item.Label": Variant("s", "TestItem"),
        "org.freedesktop.Secret.Item.Attributes": Variant("a{ss}", { "test_data": uuid.uuid4().hex }),
    }, [session_proxy.path, b"", b"TestData", "application/octet-stream"], False)

    assert on_created == [item_path]
    assert item_path in await collection_proxy.get_items() # type: ignore

    item_proxy = await bus.item(item_path)
    await item_proxy.set_label("NewLabel") # type: ignore
    assert on_changed == [item_path]

    await item_proxy.call_delete() # type: ignore
    assert on_deleted == [item_path]
    assert item_path not in await collection_proxy.get_items() # type: ignore
