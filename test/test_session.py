import os

from dbus_secrets.session import new_keypair, derive_aes_key, encode, decode

def test_cipher():
    k1, p1 = new_keypair()
    k2, p2 = new_keypair()

    aes_key = derive_aes_key(k1, p2)
    assert aes_key == derive_aes_key(k2, p1)

    plain = os.urandom(0x42)
    iv, data = encode(aes_key, plain)
    assert decode(aes_key, iv, data) == plain

    plain = "Super Secret Text"
    iv, data = encode(aes_key, plain.encode("UTF-8"))
    assert data != plain
    assert decode(aes_key, iv, data).decode("UTF-8") == plain
