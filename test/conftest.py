import pytest
import os
import uuid
import asyncio

from typing import NamedTuple, Generator, AsyncGenerator, Optional
from pathlib import Path

from dbus_next.signature import Variant
from dbus_next.aio.message_bus import MessageBus
from dbus_next.aio.proxy_object import ProxyInterface

from dbus_secrets.app import App
from dbus_secrets.store import Store
from dbus_secrets.stores import inmem, plaintext

# Override default 'function' scope for event_loop
@pytest.fixture(scope="session")
def event_loop():
    loop = asyncio.get_event_loop_policy().new_event_loop()
    yield loop
    loop.close()

# An async context manager for 'dbus-daemon'.
class DBusDaemon:

    def __init__(self, socket: Path, config: Path = Path(__file__).parent / "dbus.conf"):
        self.socket = socket
        self.config = config
        self.address = "unix:path=" + str(socket)
        self.process: Optional[asyncio.subprocess.Process] = None

    def bus(self) -> MessageBus:
        return MessageBus(self.address)

    async def __aenter__(self) -> "DBusDaemon":
        options = [
            "--config-file", self.config,
            "--nofork",
            "--nosyslog",
            "--nopidfile",
            "--address", self.address
        ]
        self.process = await asyncio.subprocess.create_subprocess_exec("dbus-daemon", *options)

        retry_count = 10
        retry_delay = 0.5
        while retry_count > 0:
            if self.socket.exists():
                break
            retry_count -= 1
            await asyncio.sleep(retry_delay)
        else:
            self.process.kill()
            raise RuntimeError("DBus socket missing after", retry_count * retry_delay, " second(s)")

        print("DBus daemon listening on '", self.address, "'", sep="")
        return self

    async def __aexit__(self, _et, _ev, _eb):
        if self.process is not None:
            self.process.kill()

class MessageBusProxy(NamedTuple):
    mbus: MessageBus
    name: str

    async def proxy(self, path: str, iface: str) -> ProxyInterface:
        i = await self.mbus.introspect(self.name, path)
        o = self.mbus.get_proxy_object(self.name, path, i)
        return o.get_interface(iface)

    async def service    (self)            -> ProxyInterface: return await self.proxy("/org/freedesktop/secrets", "org.freedesktop.Secret.Service")
    async def session    (self, path: str) -> ProxyInterface: return await self.proxy(path, "org.freedesktop.Secret.Session")
    async def collection (self, path: str) -> ProxyInterface: return await self.proxy(path, "org.freedesktop.Secret.Collection")
    async def item       (self, path: str) -> ProxyInterface: return await self.proxy(path, "org.freedesktop.Secret.Item")

@pytest.fixture(scope="session")
def dbus_socket() -> Generator[Path, None, None]:
    path = Path("/tmp/pytest-dbus-" + uuid.uuid4().hex)
    yield path
    path.unlink(missing_ok=True)

@pytest.fixture(scope="session")
async def dbus_daemon(dbus_socket: Path) -> AsyncGenerator[DBusDaemon, None]:
    async with DBusDaemon(dbus_socket) as daemon:
        yield daemon

@pytest.fixture(scope="session")
async def message_bus(dbus_daemon: DBusDaemon) -> AsyncGenerator[MessageBus, None]:
    bus = await dbus_daemon.bus().connect()
    yield bus
    bus.disconnect()

@pytest.fixture(scope='function')
def bus_name() -> str:
    name = "org.freedesktop.secrets-test-" + uuid.uuid4().hex
    print("Test bus name: '", name, "'", sep="")
    return name

@pytest.fixture(scope='function')
def bus(message_bus: MessageBus, bus_name: str) -> MessageBusProxy:
    return MessageBusProxy(message_bus, bus_name)

@pytest.fixture(scope='function')
def store_path() -> Path:
    return Path("/tmp/pytest-dbus_secrets-store-" + uuid.uuid4().hex)

@pytest.fixture(scope='function', params = ["inmem", "plaintext"])
def store(request: pytest.FixtureRequest, store_path: Path) -> Store:
    if request.param == "inmem":
        return inmem.Store()
    elif request.param == "plaintext":
        store = plaintext.Store(store_path)
        print("Test plaintext store path: '", store.path, "'", sep="")
        return store
    raise RuntimeError("invalid store name")

@pytest.fixture(scope='function')
async def app(dbus_daemon: DBusDaemon, bus_name: str, store: Store):
    async with App(store, dbus_daemon.bus(), bus_name) as app:
        yield app

# Service

@pytest.fixture(scope='function')
async def service_proxy(bus: MessageBusProxy):
    return await bus.service()

# Collection

@pytest.fixture(scope='function')
def collection_uiid() -> str:
    return uuid.uuid4().hex

@pytest.fixture(scope='function')
def collection_label(collection_uiid: str) -> str:
    return "TestCollection_" + collection_uiid

@pytest.fixture(scope='function')
async def collection_proxy(bus: MessageBusProxy, collection_label: str) -> ProxyInterface:
    service_proxy = await bus.service()
    alias = ""
    properties = {"org.freedesktop.Secret.Collection.Label": Variant("s", collection_label)}
    path, _ = await service_proxy.call_create_collection(properties, alias) # type: ignore
    return await bus.collection(path)

# Session

@pytest.fixture(scope='function')
async def session_proxy(bus: MessageBusProxy) -> ProxyInterface:
    service_proxy = await bus.service()
    _, path = await service_proxy.call_open_session("plain", Variant("ay", b"")) # type: ignore
    return await bus.session(path)

# Item

@pytest.fixture(scope='function')
def item_uiid() -> str:
    return uuid.uuid4().hex

@pytest.fixture(scope='function')
def item_label(item_uiid: str) -> str:
    return "TestItem_" + item_uiid

@pytest.fixture(scope='function')
def item_attributes(item_uiid: str) -> dict[str, str]:
    return {"test_attribute": item_uiid}

@pytest.fixture(scope='function')
def item_data() -> bytes:
    return os.urandom(240)

@pytest.fixture(scope='function')
async def item_proxy(bus: MessageBusProxy, collection_proxy: ProxyInterface, session_proxy: ProxyInterface, item_label: str, item_attributes: dict[str, str], item_data: bytes) -> ProxyInterface:
    properties = {
        "org.freedesktop.Secret.Item.Label": Variant("s", item_label),
        "org.freedesktop.Secret.Item.Attributes": Variant("a{ss}", item_attributes),
    }
    secret = [session_proxy.path, b"", item_data, "application/octet-stream"]
    path, _ = await collection_proxy.call_create_item(properties, secret, False) # type: ignore
    return await bus.item(path)
